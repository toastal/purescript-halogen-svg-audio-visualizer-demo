let WebAppManifest =
      https://gitlab.com/toastal/dhall-webmanifest/raw/trunk/Webmanifest/WebAppManifest.dhall

in    WebAppManifest::{
      , name = Some "PureScript Halogen SVG Audio Visualizer Demo"
      , short_name = Some "Audio Visualizer Demo"
      , lang = Some "en-US"
      , display = Some WebAppManifest.DisplayMode.Type.fullscreen
      , orientation = Some WebAppManifest.OrientationLock.Type.natural
      , theme_color = Some "#c0cccd"
      , background_color = Some "#121116"
      , scope = Some "/"
      }
    : WebAppManifest.Type
