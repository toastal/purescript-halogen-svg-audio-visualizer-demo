module Web.Audio.AnalyserNode
  ( AnalyserNode
  , setFftSize
  , frequencyBinCount
  , getByteFrequencyData
  ) where

-- https://developer.mozilla.org/en-US/docs/Web/API/AnalyserNode/AnalyserNode
-- Don’t forget to alias Analyser → Analyzer in Web.Audio.AnalyzerNode
import Data.ArrayBuffer.Types (Uint8Array)
import Effect.Uncurried (EffectFn2, EffectFn3, runEffectFn2, runEffectFn3)
import Effect (Effect)

foreign import data AnalyserNode ∷ Type

setProp ∷ ∀ value. String → value → AnalyserNode → Effect AnalyserNode
setProp = runEffectFn3 _setProp

foreign import _setProp ∷ ∀ value. EffectFn3 String value AnalyserNode AnalyserNode

getProp ∷ ∀ value. String → AnalyserNode → Effect value
getProp = runEffectFn2 _getProp

foreign import _getProp ∷ ∀ value. EffectFn2 String AnalyserNode value

setFftSize ∷ Int → AnalyserNode → Effect AnalyserNode
setFftSize = setProp "fftSize"

frequencyBinCount ∷ AnalyserNode → Effect Int
frequencyBinCount = getProp "frequencyBinCount"

getByteFrequencyData ∷ Uint8Array → AnalyserNode → Effect Uint8Array
getByteFrequencyData = runEffectFn2 _getByteFrequencyData

foreign import _getByteFrequencyData ∷ EffectFn2 Uint8Array AnalyserNode Uint8Array
