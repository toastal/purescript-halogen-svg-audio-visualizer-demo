module Web.Audio.AnalyzerNode
  ( AnalyzerNode
  , module Web.Audio.AnalyserNode
  ) where

-- https://developer.mozilla.org/en-US/docs/Web/API/AnalyserNode/AnalyserNode
-- This module is to alias Analyser → Analyzer from Web.Audio.AnalyserNode
import Web.Audio.AnalyserNode
  ( frequencyBinCount
  , getByteFrequencyData
  , setFftSize
  )
import Web.Audio.AnalyserNode as WAAN

type AnalyzerNode
  = WAAN.AnalyserNode
