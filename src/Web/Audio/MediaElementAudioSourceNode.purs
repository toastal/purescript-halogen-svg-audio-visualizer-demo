module Web.Audio.MediaElementAudioSourceNode where

-- https://developer.mozilla.org/en-US/docs/Web/API/MediaElementAudioSourceNode

foreign import data MediaElementAudioSourceNode ∷ Type
