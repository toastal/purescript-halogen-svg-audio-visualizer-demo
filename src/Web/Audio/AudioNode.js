/* Web.Audio.AudioNode */
"use strict";

exports._connect = function(destination, inputIndex, outputIndex, audioNode) {
  return audioNode.connect(destination, inputIndex, outputIndex);
};

exports._disconnect = function(audioNode) {
  return audioNode.disconnect();
};
