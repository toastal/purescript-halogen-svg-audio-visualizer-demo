module Web.Audio.AudioNode
  ( AudioNode
  , connect
  , disconnect
  , fromAnalyserNode
  , fromAnalyzerNode
  , fromAudioDestinationNode
  , fromMediaElementAudioSourceNode
  ) where

import Prelude
import Effect (Effect)
import Effect.Uncurried (EffectFn1, EffectFn4, runEffectFn1, runEffectFn4)
import Unsafe.Coerce (unsafeCoerce)
import Web.Audio.AnalyserNode (AnalyserNode)
import Web.Audio.AnalyzerNode (AnalyzerNode)
import Web.Audio.AudioDestinationNode (AudioDestinationNode)
import Web.Audio.MediaElementAudioSourceNode (MediaElementAudioSourceNode)

foreign import data AudioNode ∷ Type

-- TODO: first arg Coproduct with AudioParam
connect ∷ AudioNode → Int → Int → AudioNode → Effect AnalyzerNode
connect = runEffectFn4 _connect

foreign import _connect ∷ EffectFn4 AudioNode Int Int AudioNode AnalyzerNode

disconnect ∷ AudioNode → Effect Unit
disconnect = runEffectFn1 _disconnect

foreign import _disconnect ∷ EffectFn1 AudioNode Unit

fromAnalyserNode ∷ AnalyserNode → AudioNode
fromAnalyserNode = unsafeCoerce

fromAnalyzerNode ∷ AnalyzerNode → AudioNode
fromAnalyzerNode = fromAnalyserNode

fromAudioDestinationNode ∷ AudioDestinationNode → AudioNode
fromAudioDestinationNode = unsafeCoerce

fromMediaElementAudioSourceNode ∷ MediaElementAudioSourceNode → AudioNode
fromMediaElementAudioSourceNode = unsafeCoerce
