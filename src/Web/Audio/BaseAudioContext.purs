module Web.Audio.BaseAudioContext where

import Effect (Effect)
import Effect.Uncurried (EffectFn1, runEffectFn1)
import Unsafe.Coerce (unsafeCoerce)
import Web.Audio.AnalyserNode (AnalyserNode)
import Web.Audio.AnalyzerNode (AnalyzerNode)
import Web.Audio.AudioContext (AudioContext)

foreign import data BaseAudioContext ∷ Type

createAnalyser ∷ BaseAudioContext → Effect AnalyserNode
createAnalyser = runEffectFn1 _createAnalyser

createAnalyzer ∷ BaseAudioContext → Effect AnalyzerNode
createAnalyzer = createAnalyser

foreign import _createAnalyser ∷ EffectFn1 BaseAudioContext AnalyserNode

fromAudioContext ∷ AudioContext → BaseAudioContext
fromAudioContext = unsafeCoerce
