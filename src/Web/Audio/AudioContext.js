/* Web.Audio.AudioContext */
"use strict";

exports.makeAudioContext = function() {
  var AudioContext = window.AudioContext || window.webkitAudioContext;
  return new AudioContext();
};

exports._destination = function(audioContext) {
  return audioContext.destination;
};

exports._createMediaElementSource = function(media, audioContext) {
  return audioContext.createMediaElementSource(media);
};
