/* Web.Audio.BaseAudioContext */
"use strict";

exports._createAnalyser = function(baseAudioContext) {
  return baseAudioContext.createAnalyser();
};
