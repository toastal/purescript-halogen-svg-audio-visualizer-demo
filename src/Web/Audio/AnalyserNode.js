/* Web.Audio.AnalyserNode */
"use strict";

exports._setProp = function(prop, value, analyzerNode) {
  analyzerNode[prop] = value;
  return analyzerNode;
};

exports._getProp = function(prop, analyzerNode) {
  return analyzerNode[prop];
};

exports._getByteFrequencyData = function(dataArray, analyserNode) {
  analyserNode.getByteFrequencyData(dataArray);
  return dataArray;
};
