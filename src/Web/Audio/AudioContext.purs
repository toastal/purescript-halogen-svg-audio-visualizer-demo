module Web.Audio.AudioContext
  (AudioContext
  , makeAudioContext
  , destination
  , createMediaElementSource
  ) where

-- https://developer.mozilla.org/en-US/docs/Web/API/AudioContext

import Effect (Effect)
import Effect.Uncurried (EffectFn1, EffectFn2, runEffectFn1, runEffectFn2)
import Web.Audio.AudioDestinationNode (AudioDestinationNode)
import Web.Audio.MediaElementAudioSourceNode (MediaElementAudioSourceNode)
import Web.HTML.HTMLMediaElement (HTMLMediaElement)

foreign import data AudioContext ∷ Type

foreign import makeAudioContext ∷ Effect AudioContext

destination ∷ AudioContext → Effect AudioDestinationNode
destination = runEffectFn1 _destination

foreign import _destination ∷ EffectFn1 AudioContext AudioDestinationNode

createMediaElementSource ∷ HTMLMediaElement → AudioContext → Effect MediaElementAudioSourceNode
createMediaElementSource = runEffectFn2 _createMediaElementSource

foreign import _createMediaElementSource ∷ EffectFn2 HTMLMediaElement AudioContext MediaElementAudioSourceNode
