module AudioVisualizerDemo.Main (main) where

import AudioVisualizerDemo.Prelude
import AudioVisualizerDemo.Visualizer as Visualizer
import Effect (Effect)
import Effect.Aff (Aff, Fiber, launchAff)
import Effect.Exception (error)
import Halogen.Aff as HA
import Halogen.VDom.Driver (runUI)
import Web.DOM.ParentNode (QuerySelector(..))

main ∷ ∀ query output. Effect (Fiber (HA.HalogenIO query output Aff))
main = do
  launchAff do
    HA.awaitLoad
    demoElem ← do
      mfd ← HA.selectElement (QuerySelector "#demo")
      maybe (throwError (error "`#demo` element missing … but how?")) pure mfd
    runUI Visualizer.component unit demoElem
