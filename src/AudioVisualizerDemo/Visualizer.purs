module AudioVisualizerDemo.Visualizer (component) where

import AudioVisualizerDemo.Prelude
import AudioVisualizerDemo.Icon as I
import AudioVisualizerDemo.Picker as Picker
import AudioVisualizerDemo.Player as Player
import AudioVisualizerDemo.Visualization (Visualization)
import AudioVisualizerDemo.Visualization as Visualization
import Data.ArrayBuffer.Typed as TypedArray
import Data.ArrayBuffer.Types (Uint8Array)
import Effect.Ref as Ref
import Effect.Unsafe (unsafePerformEffect)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP
import Halogen.Query.EventSource as HES
import Web.Audio.AnalyzerNode (AnalyzerNode)
import Web.Audio.AnalyzerNode as AnalyzerNode
import Web.Audio.AudioContext as AudioContext
import Web.Audio.AudioNode (AudioNode)
import Web.Audio.AudioNode as AudioNode
import Web.Audio.BaseAudioContext as BaseAudioContext
import Web.HTML as HTML
import Web.HTML.HTMLAudioElement (toHTMLMediaElement) as HTMLAudioElement
import Web.HTML.Window as Window

type Slots
  = ( picker ∷ ∀ query. H.Slot query Picker.Output Unit
    , player ∷ ∀ query. H.Slot query Player.Output Unit
    )

_picker = SProxy ∷ SProxy "picker"

_player = SProxy ∷ SProxy "player"

type State
  = { subscription ∷ Maybe H.SubscriptionId
    , analyzer ∷ Maybe AnalyzerNode
    , dataArray ∷ Uint8Array
    , visualization ∷ Visualization
    }

initialState ∷ ∀ input. input → State
initialState _ =
  { subscription: Nothing
  , analyzer: Nothing
  , dataArray: unsafePerformEffect (TypedArray.empty 0)
  , visualization: Visualization.defaultVisualization
  }

data Action
  = Initialize
  | Finalize
  | Tick
  | HandlePicker Picker.Output
  | HandlePlayer Player.Output

component ∷ ∀ query input output m. MonadAff m ⇒ H.Component HH.HTML query input output m
component =
  H.mkComponent
    { initialState
    , render
    , eval:
        H.mkEval
          $ H.defaultEval
              { handleAction = handleAction
              , initialize = Just Initialize
              , finalize = Just Finalize
              }
    }
  where
  render ∷ State → H.ComponentHTML Action Slots m
  render state =
    HH.div
      [ HP.id_ "Visualizer"
      , HP.class_ $ HH.ClassName "Visualizer"
      ]
      [ HH.div
          [ HP.class_ $ HH.ClassName "PlayerPickers" ]
          [ HH.slot _picker unit Picker.component unit (Just ∘ HandlePicker)
          , HH.slot _player unit Player.component unit (Just ∘ HandlePlayer)
          ]
      , HH.fromPlainHTML $ (Visualization.spec state.visualization).render state.dataArray
      , HH.fromPlainHTML renderSeeProject
      ]

  renderSeeProject ∷ HH.PlainHTML
  renderSeeProject =
    HH.a
      [ HP.href "https://gitlab.com/toastal/purescript-halogen-svg-audio-visualizer-demo"
      , HP.class_ $ HH.ClassName "SeeProject"
      , HP.title "See project on GitLab"
      ]
      [ I.gitlab
      , HH.span
          [ HP.class_ $ HH.ClassName "srOnly" ]
          [ HH.text "See project on GitLab" ]
      ]

handleAction ∷ ∀ output m. MonadAff m ⇒ Action → H.HalogenM State Action Slots output m Unit
handleAction = case _ of
  Tick → do
    { analyzer, dataArray } ← H.get
    case analyzer of
      Nothing → pure unit
      Just alzr → do
        darr ← liftEffect $ AnalyzerNode.getByteFrequencyData dataArray alzr
        H.modify_ (_ { dataArray = darr })
  HandlePlayer output → case output of
    Player.ChangedAudio audio → do
      H.gets _.analyzer
        >>= traverse_ (liftEffect ∘ AudioNode.disconnect ∘ AudioNode.fromAnalyzerNode)
      audioCtx ← liftEffect $ AudioContext.makeAudioContext
      source ← liftEffect $ AudioContext.createMediaElementSource (HTMLAudioElement.toHTMLMediaElement audio) audioCtx
      analyzer ←
        liftEffect do
          BaseAudioContext.createAnalyzer (BaseAudioContext.fromAudioContext audioCtx)
            >>= AnalyzerNode.setFftSize 256
      let
        analyzerAudioNode ∷ AudioNode
        analyzerAudioNode = AudioNode.fromAnalyzerNode analyzer
      dataArray ←
        liftEffect do
          _ ← AudioNode.connect analyzerAudioNode 0 0 (AudioNode.fromMediaElementAudioSourceNode source)
          dest ← AudioContext.destination audioCtx
          _ ← AudioNode.connect (AudioNode.fromAudioDestinationNode dest) 0 0 analyzerAudioNode
          fbc ← AnalyzerNode.frequencyBinCount analyzer
          TypedArray.empty fbc
      H.modify_ (_ { analyzer = pure analyzer, dataArray = dataArray })
  HandlePicker viz → do
    H.modify_ (_ { visualization = viz })
  Initialize → do
    handleAction Finalize
    subscription ←
      H.subscribe
        $ HES.effectEventSource \emitter → do
            ref ← Ref.new Nothing
            let
              loop = do
                HES.emit emitter Tick
                id ← Window.requestAnimationFrame loop =<< HTML.window
                Ref.write (Just id) ref
            loop
            pure
              $ HES.Finalizer do
                  Ref.read ref
                    >>= traverse_ \id →
                        HTML.window >>= Window.cancelAnimationFrame id
    H.modify_ (_ { subscription = pure subscription })
  Finalize → do
    { subscription, analyzer } ← H.get
    for_ subscription \sub → do
      H.unsubscribe sub
      H.modify (_ { subscription = Nothing })
    for_ analyzer \alzr → do
      _ ← liftEffect $ AudioNode.disconnect (AudioNode.fromAnalyzerNode alzr)
      H.modify (_ { analyzer = Nothing })
