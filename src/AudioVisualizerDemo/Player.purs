module AudioVisualizerDemo.Player (Output(..), component) where

import AudioVisualizerDemo.Prelude
import AudioVisualizerDemo.Icon as I
import DOM.HTML.Indexed.InputAcceptType (mediaType)
import Data.Lens.Record (prop)
import Data.Maybe (isNothing)
import Data.MediaType (MediaType(..))
import Data.Number as Number
import Effect.Unsafe (unsafePerformEffect)
import Foreign (Foreign)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.Query.EventSource as HES
import JSMediaTags.Reader as MediaTagsReader
import Record as Record
import Simple.JSON as JSON
import Web.Event.Event (EventType(..))
import Web.Event.Event as WEE
import Web.Event.EventTarget (EventTarget)
import Web.File.Blob (Blob)
import Web.File.File (File)
import Web.File.File as File
import Web.File.FileList as WFFL
import Web.File.Url (createObjectURL, revokeObjectURL)
import Web.HTML.HTMLAudioElement (HTMLAudioElement)
import Web.HTML.HTMLAudioElement as HTMLAudioElement
import Web.HTML.HTMLInputElement as HTMLInputElement
import Web.HTML.HTMLMediaElement (HTMLMediaElement)
import Web.HTML.HTMLMediaElement as HTMLMediaElement

data Output
  = ChangedAudio HTMLAudioElement

type State
  = { audio ∷ Maybe AudioState
    }

_audio = SProxy ∷ SProxy "audio"

type Tags
  = { title ∷ String
    , album ∷ String
    , artist ∷ String
    }

readTags ∷ Foreign -> JSON.E Tags
readTags f = do
  (intermediate ∷ { tags ∷ Tags }) ← JSON.read f
  pure $ Record.get (SProxy ∷ SProxy "tags") intermediate

type AudioState
  = { elem ∷ HTMLAudioElement
    , currentTime ∷ Number
    , timeUpdateSubID ∷ H.SubscriptionId
    , tags ∷ Maybe Tags
    }

_elem = SProxy ∷ SProxy "elem"

_currentTime = SProxy ∷ SProxy "currentTime"

_timeUpdateSubID = SProxy ∷ SProxy "timeUpdateSubID"

_tags = SProxy ∷ SProxy "tags"

initialState ∷ ∀ input. input → State
initialState _ =
  { audio: Nothing
  }

data Action
  = Initialize
  | Finalize
  | FileChange File
  | AudioPlay
  | AudioPause
  | AudioTimeUpdate Number
  | AudioSeek Number

component ∷ ∀ query input m. MonadAff m ⇒ H.Component HH.HTML query input Output m
component =
  H.mkComponent
    { initialState
    , render
    , eval:
        H.mkEval
          $ H.defaultEval
              { handleAction = handleAction
              , initialize = Just Initialize
              , finalize = Just Finalize
              }
    }
  where
  render ∷ State → H.ComponentHTML Action () m
  render state =
    HH.div
      [ HP.class_ $ HH.ClassName "Player-container" ]
      [ renderPlayerPicker state.audio
      , renderSeeker state.audio
      ]

  renderPlayerPicker ∷ Maybe AudioState → HH.HTML (H.ComponentSlot HH.HTML () m Action) Action
  renderPlayerPicker audio =
    HH.div
      [ HP.class_ $ HH.ClassName "Player"
      ]
      [ HH.label
          [ HP.class_ $ HH.ClassName "Player-label Player-label--filePicker" ]
          [ HH.span
              [ HP.class_ $ HH.ClassName "PlayerPicker-labelText srOnly" ]
              [ HH.text "Pick an audio file" ]
          , HH.span
              [ HP.class_ $ HH.ClassName "Button Player-filePickerFalseButton"
              , HP.title "Pick an audio file"
              ]
              [ I.folderOpenBassClef ]
          , HH.input
              [ HP.type_ HP.InputFile
              , HP.class_ $ HH.ClassName "Player-filePicker"
              , HP.accept (mediaType (MediaType "audio/*"))
              , HE.onChange fileChangeHandler
              ]
          ]
      , HH.button
          [ HP.type_ HP.ButtonButton
          , HP.class_ $ HH.ClassName "Player-play"
          , HP.title (if isPlaying then "Pause" else "Play")
          , HP.disabled (isNothing audio)
          , HE.onClick
              ( \_ →
                  map
                    ( \{ elem } →
                        if unsafeMediaEff HTMLMediaElement.paused elem then
                          AudioPlay
                        else
                          AudioPause
                    )
                    audio
              )
          ]
          [ if isPlaying then I.pause else I.play ]
      , maybe (HH.text "") renderTags (_.tags =<< audio)
      ]
    where
    isPlaying ∷ Boolean
    isPlaying = maybe false (not ∘ unsafeMediaEff HTMLMediaElement.paused ∘ _.elem) audio

    fileChangeHandler ∷ WEE.Event → Maybe Action
    fileChangeHandler evt = do
      target ← HTMLInputElement.fromEventTarget =<< WEE.target evt
      fileList ← unsafePerformEffect $ HTMLInputElement.files target
      file ← WFFL.item 0 fileList
      pure $ FileChange file

  renderSeeker ∷ Maybe AudioState → HH.HTML (H.ComponentSlot HH.HTML () m Action) Action
  renderSeeker audio =
    HH.label
      [ HP.class_ $ HH.ClassName "Player-label Player-label--audioSeeker" ]
      [ HH.span
          [ HP.class_ $ HH.ClassName "PlayerPicker-labelText srOnly" ]
          [ HH.text "Current time / seek to" ]
      , HH.input
          [ HP.type_ HP.InputRange
          , HP.class_ $ HH.ClassName "Player-seeker"
          , HP.min 0.0
          , HP.max (maybe 0.0 (unsafeMediaEff HTMLMediaElement.duration ∘ _.elem) audio)
          , HP.step (HP.Step 0.2)
          , HP.disabled (isNothing audio)
          , HP.value (maybe "0.0" (show ∘ _.currentTime) audio)
          , HE.onInput seekHandler
          ]
      ]
    where
    seekHandler ∷ WEE.Event → Maybe Action
    seekHandler evt = do
      target ← HTMLInputElement.fromEventTarget =<< WEE.target evt
      time ← Number.fromString $ unsafePerformEffect $ HTMLInputElement.value target
      pure $ AudioSeek time

  renderTags ∷ Tags → HH.HTML (H.ComponentSlot HH.HTML () m Action) Action
  renderTags tags =
    HH.dl
      [ HP.class_ $ HH.ClassName "PlayerPicker-tags Tags"
      ]
      [ HH.dt [ HP.class_ $ HH.ClassName "Tags-title srOnly" ]
          [ HH.text "Title" ]
      , HH.dd [ HP.class_ $ HH.ClassName "Tags-item Tags-item--title" ]
          [ HH.text tags.title ]
      , HH.dt [ HP.class_ $ HH.ClassName "Tags-title srOnly" ]
          [ HH.text "Album" ]
      , HH.dd [ HP.class_ $ HH.ClassName "Tags-item Tags-item--album" ]
          [ HH.text tags.album ]
      , HH.dt [ HP.class_ $ HH.ClassName "Tags-title srOnly" ]
          [ HH.text "Artist" ]
      , HH.dd [ HP.class_ $ HH.ClassName "Tags-item Tags-item--artist" ]
          [ HH.text tags.artist ]
      ]

  handleAction ∷ Action → H.HalogenM State Action () Output m Unit
  handleAction = case _ of
    AudioTimeUpdate t → do
      H.modify_ (prop _audio ∘ _Just ∘ prop _currentTime .~ t)
    AudioSeek t → do
      H.gets _.audio >>= traverse_ (liftMediaEff (HTMLMediaElement.setCurrentTime t))
      H.modify_ (prop _audio ∘ _Just ∘ prop _currentTime .~ t)
    AudioPlay → do
      H.gets _.audio >>= traverse_ (liftMediaEff HTMLMediaElement.play)
    AudioPause → do
      H.gets _.audio >>= traverse_ (liftMediaEff HTMLMediaElement.pause)
    FileChange file → do
      let
        blob :: Blob
        blob = (File.toBlob file)
      fileURL ← liftEffect $ createObjectURL blob
      audio ← do
        maudio ← H.gets _.audio
        for_ maudio \audio' → do
          H.unsubscribe audio'.timeUpdateSubID
        elem ←
          liftEffect do
            aud ←
              -- create new one if missing, otherwise recycle
              ( case maudio of
                  Nothing → HTMLAudioElement.create unit
                  Just { elem } → do
                    _ ← HTMLMediaElement.pause mel
                    src ← HTMLMediaElement.src mel
                    _ ← revokeObjectURL src
                    pure elem
                    where
                    mel ∷ HTMLMediaElement
                    mel = HTMLAudioElement.toHTMLMediaElement elem
              )
            let
              mel ∷ HTMLMediaElement
              mel = HTMLAudioElement.toHTMLMediaElement aud
            _ ← HTMLMediaElement.setSrc fileURL mel
            _ ← HTMLMediaElement.load mel
            pure aud
        timeUpdateSubID ← H.subscribe =<< liftEffect (audioTimeUpdateEventSource elem)
        tags ← do
          reader ←
            liftEffect
              $ MediaTagsReader.createFromBlob blob
              >>= MediaTagsReader.setTagsToRead [ "title", "album", "artist" ]
          frgn ← H.liftAff $ MediaTagsReader.read reader
          pure $ either (\_ → Nothing) Just (readTags frgn)
        pure { elem, timeUpdateSubID, currentTime: 0.0, tags }
      H.modify_ (_ { audio = pure audio })
      H.raise (ChangedAudio audio.elem)
      void $ liftMediaEff HTMLMediaElement.play audio
    Initialize → do
      handleAction Finalize
    Finalize → do
      { audio } ← H.get
      for_ audio \audio' → do
        H.unsubscribe audio'.timeUpdateSubID
        _ ← liftMediaEff HTMLMediaElement.pause audio'
        _ ←
          liftEffect do
            src ← HTMLMediaElement.src (HTMLAudioElement.toHTMLMediaElement audio'.elem)
            revokeObjectURL src
        H.modify (_ { audio = Nothing })
    where
    liftMediaEff ∷ ∀ a. (HTMLMediaElement → Effect a) → AudioState → H.HalogenM State Action () Output m a
    liftMediaEff eff = liftEffect ∘ eff ∘ HTMLAudioElement.toHTMLMediaElement ∘ _.elem

-- HACK: I'm gonna do a lot of side-effects across the HTMLAudioElement
unsafeMediaEff ∷ ∀ a. (HTMLMediaElement → Effect a) → HTMLAudioElement → a
unsafeMediaEff eff = unsafePerformEffect ∘ eff ∘ HTMLAudioElement.toHTMLMediaElement

audioTimeUpdateEventSource ∷ ∀ m. MonadAff m ⇒ HTMLAudioElement → Effect (HES.EventSource m Action)
audioTimeUpdateEventSource audioElem = do
  pure $ HES.eventListenerEventSource (EventType "timeupdate") audioEventTarget handler
  where
  audioEventTarget ∷ EventTarget
  audioEventTarget = HTMLAudioElement.toEventTarget audioElem

  handler ∷ WEE.Event → Maybe Action
  handler evt = do
    target ← HTMLMediaElement.fromEventTarget =<< WEE.target evt
    pure (AudioTimeUpdate (unsafePerformEffect (HTMLMediaElement.currentTime target)))
