module AudioVisualizerDemo.Visualization where

import AudioVisualizerDemo.Prelude
import AudioVisualizerDemo.Visualization.Blob as Blob
import AudioVisualizerDemo.Visualization.CenteredBars as CenteredBars
import AudioVisualizerDemo.Visualization.SpikeyCircle as SpikeyCircle
import AudioVisualizerDemo.Visualization.Spec (Spec)
import Data.Variant as V

type Visualization
  = Variant
      ( blob ∷ Unit
      , centeredBars ∷ Unit
      , spikeyCircle ∷ Unit
      )

defaultVisualization ∷ Visualization
defaultVisualization = centeredBars

blob ∷ ∀ v. Variant ( blob ∷ Unit | v )
blob = V.inj _blob unit

centeredBars ∷ ∀ v. Variant ( centeredBars ∷ Unit | v )
centeredBars = V.inj _centeredBars unit

spikeyCircle ∷ ∀ v. Variant ( spikeyCircle ∷ Unit | v )
spikeyCircle = V.inj _spikeyCircle unit

_blob = SProxy ∷ SProxy "blob"

_centeredBars = SProxy ∷ SProxy "centeredBars"

_spikeyCircle = SProxy ∷ SProxy "spikeyCircle"

spec ∷ Visualization → Spec
spec =
  V.case_
    # V.on _blob (const Blob.spec)
    # V.on _centeredBars (const CenteredBars.spec)
    # V.on _spikeyCircle (const SpikeyCircle.spec)
