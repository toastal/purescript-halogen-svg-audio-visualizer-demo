module AudioVisualizerDemo.Visualization.SpikeyCircle where

import AudioVisualizerDemo.Prelude
import AudioVisualizerDemo.Helpers (iterateData)
import AudioVisualizerDemo.Visualization.Spec (Spec)
import Data.ArrayBuffer.Typed as TypedArray
import Data.ArrayBuffer.Types (Uint8Array)
import Data.Foldable (intercalate, surroundMap)
import Data.Int (ceil, toNumber)
import Data.String as String
import Data.UInt as UInt
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP

spec ∷ Spec
spec = { title, render }

title ∷ String
title = "Spikey circle"

render ∷ Uint8Array → HH.PlainHTML
render dataArray =
  s (HH.ElemName "svg")
    [ HH.attr (HH.AttrName "class") "Visualization Visualization--centeredBars"
    , HH.attr (HH.AttrName "viewBox") viewBox'
    , HH.attr (HH.AttrName "preserveAspectRatio") "xMidYMid meet"
    ]
    [ s (HH.ElemName "g")
        []
        [ s (HH.ElemName "path")
            [ HH.attr (HH.AttrName "d")
                ( dataArray
                    # iterateData dValue
                    # intercalate " L"
                    # \str → "M" <> str <> " Z"
                )
            , HH.attr (HH.AttrName "fill") "none"
            , HH.attr (HH.AttrName "stroke") "#fff"
            , HH.attr (HH.AttrName "stroke-width") "2"
            ]
            []
        ]
    ]
  where
  s = HH.elementNS $ HH.Namespace "http://www.w3.org/2000/svg"

  minRadius ∷ Int
  minRadius = 64

  maxRadius ∷ Int
  maxRadius = 256 + minRadius

  halfMaxRadius ∷ Int
  halfMaxRadius = ceil (toNumber maxRadius * 0.5)

  viewBox' ∷ String
  viewBox' = surroundMap " " show [ a, a, b, b ]
    where
    a ∷ Int
    a = maxRadius * -1

    b ∷ Int
    b = maxRadius * 2

  dataLength ∷ Int
  dataLength = TypedArray.length dataArray

  dataRadOffsetPer ∷ Number
  dataRadOffsetPer =
    if dataLength == 0 then
      0.0
    else
      tau / toNumber dataLength

  dValue ∷ UInt.UInt → Int → String
  dValue uint idx = show (cal cos) <> "," <> show (cal sin)
    where
    v ∷ Number
    v = toNumber (UInt.toInt uint + minRadius)

    cal ∷ (Number -> Number) → Number
    cal fn = v * fn ((dataRadOffsetPer * toNumber idx) + pi)
