module AudioVisualizerDemo.Visualization.Spec where

import Data.ArrayBuffer.Types (Uint8Array)
import Halogen.HTML as HH

type Spec =
  { title ∷ String
  , render ∷ Uint8Array → HH.PlainHTML
  }
