module AudioVisualizerDemo.Visualization.CenteredBars where

import AudioVisualizerDemo.Prelude
import Data.ArrayBuffer.Typed as TypedArray
import Data.ArrayBuffer.Types (Uint8Array)
import Data.Int (toNumber)
import Data.UInt as UInt
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP
import AudioVisualizerDemo.Helpers (iterateData)
import AudioVisualizerDemo.Visualization.Spec (Spec)

spec ∷ Spec
spec = { title, render }

title ∷ String
title = "Centered bars"

render ∷ Uint8Array → HH.PlainHTML
render dataArray =
  s (HH.ElemName "svg")
    [ HH.attr (HH.AttrName "class") "Visualization Visualization--centeredBars"
    , HH.attr (HH.AttrName "viewBox") ("0 0 " <> show dataLength <> " " <> show maxHeight)
    , HH.attr (HH.AttrName "preserveAspectRatio") "none"
    ]
    [ sk (HH.ElemName "g")
        []
        (iterateData renderRect dataArray)
    ]
  where
  s = HH.elementNS $ HH.Namespace "http://www.w3.org/2000/svg"

  sk = HH.keyedNS $ HH.Namespace "http://www.w3.org/2000/svg"

  maxHeight ∷ Int
  maxHeight = 256

  dataLength ∷ Int
  dataLength = TypedArray.length dataArray

  renderRect ∷ UInt.UInt → Int → String × HH.PlainHTML
  renderRect uint idx =
    Tuple (show idx)
      $ s (HH.ElemName "rect")
          [ HH.attr (HH.AttrName "x") (show idx)
          , HH.attr (HH.AttrName "y") (show (0.5 * toNumber (maxHeight - v)))
          , HP.attr (HH.AttrName "width") "1"
          , HP.attr (HH.AttrName "height") (show v)
          , HH.attr (HH.AttrName "fill") ("hsl(" <> show hue <> "," <> show saturation <> "%," <> show lightness <> "%)")
          ]
          []
    where
    v ∷ Int
    v = UInt.toInt uint

    perc ∷ Number
    perc = toNumber v / toNumber maxHeight

    hue ∷ Number
    hue = perc * 20.0 + 260.0

    saturation ∷ Number
    saturation = perc * 30.0 + 70.0

    lightness ∷ Number
    lightness = perc * 40.0 + 30.0
