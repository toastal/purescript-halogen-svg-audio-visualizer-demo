module AudioVisualizerDemo.Visualization.Blob where

import AudioVisualizerDemo.Prelude
import Data.ArrayBuffer.Typed as TypedArray
import Data.ArrayBuffer.Types (Uint8Array)
import Data.Int (toNumber)
import Data.UInt as UInt
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP
import AudioVisualizerDemo.Helpers (iterateData)
import AudioVisualizerDemo.Visualization.Spec (Spec)

spec ∷ Spec
spec = { title, render }

title ∷ String
title = "Blob"

render ∷ Uint8Array → HH.PlainHTML
render dataArray =
  s (HH.ElemName "svg")
    [ HH.attr (HH.AttrName "class") "Visualization Visualization--gooeyBars"
    , HH.attr (HH.AttrName "viewBox") ("0 0 " <> show maxWidth <> " " <> show dataLength)
    , HH.attr (HH.AttrName "preserveAspectRatio") "xMidYMid meet"
    ]
    [ sk (HH.ElemName "g")
        [ HH.attr (HH.AttrName "filter") "url('#gooeyEffect')"
        , HH.attr (HH.AttrName "fill") ("hsl(180,100%,80%)")
        ]
        (iterateData renderRect dataArray)
    ]
  where
  s = HH.elementNS $ HH.Namespace "http://www.w3.org/2000/svg"

  sk = HH.keyedNS $ HH.Namespace "http://www.w3.org/2000/svg"

  maxWidth ∷ Int
  maxWidth = 256

  dataLength ∷ Int
  dataLength = TypedArray.length dataArray

  renderRect ∷ UInt.UInt → Int → String × HH.PlainHTML
  renderRect uint idx =
    Tuple (show idx)
      $ s (HH.ElemName "rect")
          [ HH.attr (HH.AttrName "x") (show (0.5 * toNumber (maxWidth - v)))
          , HH.attr (HH.AttrName "y") (show (dataLength - idx))
          , HP.attr (HH.AttrName "width") (show v)
          , HP.attr (HH.AttrName "height") "1"
          ]
          []
    where
    v ∷ Int
    v = UInt.toInt uint
