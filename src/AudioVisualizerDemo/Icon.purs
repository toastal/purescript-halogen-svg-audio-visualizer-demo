module AudioVisualizerDemo.Icon where

import AudioVisualizerDemo.Prelude

import Halogen.HTML as HH
import Halogen.HTML.Properties.ARIA as Aria

foreign import iconSVGURL ∷ String

i ∷ ∀ p i. String → String → HH.HTML p i
i id viewBox =
  s (HH.ElemName "svg")
    [ HH.attr (HH.AttrName "class") ("Icon Icon--" <> id)
    , HH.attr (HH.AttrName "viewBox") viewBox
    , Aria.hidden "true"
    ]
    [ s (HH.ElemName "use")
        [ xlinkAttr (HH.AttrName "xlink:href") (iconSVGURL <> "#" <> id) ]
        []
    ]
  where
  s = HH.elementNS $ HH.Namespace "http://www.w3.org/2000/svg"

  xlinkAttr = HH.attrNS $ HH.Namespace "http://www.w3.org/1999/xlink"

folderOpen ∷ ∀ p i. HH.HTML p i
folderOpen = i "folderOpen" "0 0 32 32"

folderOpenBassClef ∷ ∀ p i. HH.HTML p i
folderOpenBassClef = i "folderOpen--bassCleff" "0 0 32 32"

tv ∷ ∀ p i. HH.HTML p i
tv = i "tv" "0 0 32 32"

play ∷ ∀ p i. HH.HTML p i
play = i "play" "0 0 32 32"

pause ∷ ∀ p i. HH.HTML p i
pause = i "pause" "0 0 32 32"

volumeHigh ∷ ∀ p i. HH.HTML p i
volumeHigh = i "volumeHigh" "0 0 32 34"

volumeMedium ∷ ∀ p i. HH.HTML p i
volumeMedium = i "volumeMedium" "0 0 32 32"

volumeLow ∷ ∀ p i. HH.HTML p i
volumeLow = i "volumeLow" "0 0 32 32"

volumeMute ∷ ∀ p i. HH.HTML p i
volumeMute = i "volumeMute" "0 0 32 32"

gitlab ∷ ∀ p i. HH.HTML p i
gitlab = i "gitlab" "0 0 24 24"
