module AudioVisualizerDemo.Picker (Output(..), component) where

import AudioVisualizerDemo.Prelude
import AudioVisualizerDemo.Icon as I
import AudioVisualizerDemo.Visualization (Visualization)
import AudioVisualizerDemo.Visualization as Visualization
import Data.Enum (fromEnum, toEnum, upFromIncluding)
import Data.Int as Int
import Effect.Unsafe (unsafePerformEffect)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Web.Event.Event as WEE
import Web.HTML.HTMLSelectElement as HTMLSelectElement

type Output
  = Visualization

type State
  = Visualization

initialState ∷ ∀ input. input → State
initialState _ = Visualization.defaultVisualization

type Action
  = Visualization

component ∷ ∀ query input m. MonadAff m ⇒ H.Component HH.HTML query input Output m
component =
  H.mkComponent
    { initialState
    , render
    , eval:
        H.mkEval
          $ H.defaultEval
              { handleAction = handleAction
              }
    }
  where
  render ∷ Visualization → HH.HTML (H.ComponentSlot HH.HTML () m Action) Action
  render viz =
    HH.label
      [ HP.class_ $ HH.ClassName "Picker Picker-label" ]
      [ HH.span_ [ I.tv ]
      , HH.span
          [ HP.class_ $ HH.ClassName "PlayerPicker-labelText srOnly" ]
          [ HH.text "Select visualization" ]
      , HH.select
          [ HP.class_ $ HH.ClassName "VisualizationSelect"
          , HE.onChange changeHandler
          ]
          (map renderOption (upFromIncluding (bottom ∷ Visualization)))
      ]
    where
    renderOption ∷ ∀ p i. Visualization → HH.HTML p i
    renderOption viz' =
      HH.option
        [ HP.value $ show $ fromEnum viz'
        , HP.selected $ viz == viz'
        , HP.disabled $ viz' == Visualization.blob
        ]
        [ HH.text $ (Visualization.spec viz').title ]

    changeHandler ∷ WEE.Event → Maybe Action
    changeHandler evt = do
      target ← HTMLSelectElement.fromEventTarget =<< WEE.target evt
      value ← Int.fromString $ unsafePerformEffect $ HTMLSelectElement.value target
      toEnum value

  handleAction ∷ Action → H.HalogenM State Action () Output m Unit
  handleAction viz = do
    H.put viz
    H.raise viz
