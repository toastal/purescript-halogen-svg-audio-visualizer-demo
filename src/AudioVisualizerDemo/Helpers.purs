module AudioVisualizerDemo.Helpers where

import AudioVisualizerDemo.Prelude
import Data.Array.ST as STA
import Data.Array.ST.Extra as STAx
import Data.Array.ST.Iterator as STAI
import Data.ArrayBuffer.Typed as TypedArray
import Data.ArrayBuffer.Types (Uint8Array)
import Data.UInt (UInt)
import Effect.Unsafe (unsafePerformEffect)

iterateData ∷ ∀ a. (UInt → Int → a) → Uint8Array → Array a
iterateData fn dataArray =
  STA.run do
    acc ← STAx.sizedEmpty (TypedArray.length dataArray)
    iter ← STAI.iterator mindexedTuple
    STAI.iterate iter \(uint × idx) → void $ STA.modify idx (const $ fn uint idx) acc
    pure acc
  where
  mindexedTuple ∷ Int → Maybe (UInt × Int)
  mindexedTuple idx = (_ × idx) <$> unsafePerformEffect (TypedArray.at dataArray idx)
