/* JSMediaTags.Reader */
"use strict";

var Reader = require("jsmediatags").Reader;

exports._createFromBlob = function(blob) {
  var reader = new Reader(blob);
  return reader;
};

exports._setTagToRead = function(tags, reader) {
  return reader.setTagsToRead(tags);
};

exports._read = function(reader) {
  return function(onError, onSuccess) {
    reader.read({ onSuccess: onSuccess, onError: onError });

    return function(cancelError, onCancelerError, onCancelerSuccess) {
      // https://github.com/aadsm/jsmediatags/issues/131
      var fr = reader._getFileReader();
      if (typeof fr.abort === "function") {
        fr.abort();
        onCancelerSuccess();
      } else {
        onCancelerError();
      }
    };
  };
};
