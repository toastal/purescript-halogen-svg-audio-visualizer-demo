module JSMediaTags.Reader
  ( Reader
  , createFromBlob
  , setTagsToRead
  , read
  ) where

import Prelude
import Effect (Effect)
import Effect.Aff (Aff)
import Effect.Aff.Compat
  ( EffectFn1
  , EffectFn2
  , EffectFnAff
  , fromEffectFnAff
  , runEffectFn1
  , runEffectFn2
  )
import Foreign (Foreign)
import Web.File.Blob (Blob)

foreign import data Reader ∷ Type

createFromBlob ∷ Blob → Effect Reader
createFromBlob = runEffectFn1 _createFromBlob

foreign import _createFromBlob ∷ EffectFn1 Blob Reader

setTagsToRead ∷ Array String → Reader → Effect Reader
setTagsToRead = runEffectFn2 _setTagToRead

foreign import _setTagToRead ∷ EffectFn2 (Array String) Reader Reader

read ∷ Reader → Aff Foreign
read = fromEffectFnAff <<< _read

foreign import _read ∷ Reader → EffectFnAff Foreign
