/* Data.Array.ST.Extra */
"use strict";

exports.sizedEmpty = function(size) {
  return function() {
    return new Array(size);
  };
};
