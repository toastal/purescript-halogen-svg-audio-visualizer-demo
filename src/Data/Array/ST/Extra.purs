module Data.Array.ST.Extra (sizedEmpty) where

import Control.Monad.ST (ST)
import Data.Array.ST (STArray)

foreign import sizedEmpty ∷ ∀ h a. Int → ST h (STArray h a)
