{ name = "halogen-svg-audio-visualizer-demo"
, dependencies =
  [ "argonaut-codecs"
  , "argonaut-core"
  , "arraybuffer"
  , "console"
  , "debug"
  , "effect"
  , "either"
  , "enums"
  , "foreign"
  , "generics-rep"
  , "halogen"
  , "math"
  , "maybe"
  , "numbers"
  , "partial"
  , "prelude"
  , "psci-support"
  , "record"
  , "simple-json"
  , "variant"
  , "web-dom"
  , "web-file"
  ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
