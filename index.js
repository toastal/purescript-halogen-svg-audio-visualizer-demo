import { description, version } from "./package.json"

console.log("🎧", description, `v${version}`)

let app

function main() {
  if (process.env.NODE_ENV === "production") {
    app = require("./dce-output/AudioVisualizerDemo.Main/index.js").main()
  } else {
    app = require("./output/AudioVisualizerDemo.Main/index.js").main()
  }
}

if (module.hot) {
  module.hot.accept(function() {
    app != null && typeof app.kill === "function" && app.kill()
    Array.prototype.forEach.call(
      document.querySelectorAll("#demo>div"),
      function(d) { d.remove() },
    )
    main()
  })
}

main()
