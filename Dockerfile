FROM debian:bullseye-slim

# https://github.com/asdf-community/asdf-ubuntu/blob/master/Dockerfile

LABEL maintainer="toastal <toastal@protonmail.com>"
LABEL updated_at=2020-11-28

RUN apt-get update && \
    apt-get dist-upgrade -y && \
    apt-get install -y bash bzip2 ca-certificates curl git gnupg libc6 libtinfo5 wget tree

SHELL ["/bin/bash", "-l", "-c"]

RUN git clone --depth 1 https://github.com/asdf-vm/asdf.git /asdf && \
    echo -e '\n. /asdf/asdf.sh' >> ~/.bashrc && \
    echo -e '\n. /asdf/asdf.sh' >> ~/.profile && \
    echo -e 'legacy_version_file = no' > ~/.asdfrc && \
    source ~/.bashrc

RUN asdf plugin-add nodejs && \
    bash -c '${ASDF_DATA_DIR:=$HOME/.asdf}/plugins/nodejs/bin/import-release-team-keyring' && \
    asdf plugin-add yarn && \
    asdf plugin-add dhall && \
    asdf plugin-add purescript-zephyr https://github.com/instateam/asdf-purescript-zephyr.git && \
    asdf update --head

ENV PATH "${PATH}:/asdf/shims:/asdf/bin/:.asdf/shims/"
