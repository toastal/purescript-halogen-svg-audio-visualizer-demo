let upstream =
      https://github.com/purescript/package-sets/releases/download/psc-0.13.8-20201125/packages.dhall sha256:ef58d9afae22d1bc9d83db8c72d0a4eca30ce052ab49bbc44ced2da0bc5cad1a

let overrides =
      { web-html =
            upstream.web-html
          ⫽ { repo = "https://github.com/toastal/purescript-web-html.git"
            , version = "80870f38a2aef72cd0e5e5269551227cfb6f33d6"
            }
      }

let additions = {=}

in  upstream ⫽ overrides ⫽ additions
